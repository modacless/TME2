;;;; Exercice 2 : Moyennes variées

#lang racket

(require "ex1.scm")

(provide myreduce)

;;; Q1

(define (sum l)
  (if (pair? l)
      (+ (car l) (sum (cdr l)))
      0))

(sum (intervalle 1 11))

(define (average1 l)
  (if (pair? l)
      (/ (sum l) (length l))
      0))

(average1 (intervalle 1 11))

;;; Q2

(define (avg2-aux l s n)
  (if (pair? l)
      (avg2-aux (cdr l) (+ s (car l)) (+ n 1))
      (if (zero? n) 0 (/ s n))))

(define (average2 l)
  (avg2-aux l 0 0))

(average2 (intervalle 1 11))

;;; Q3

(define (average3 l)
  (define (avg3-aux l s n)
    (if (pair? l)
        (avg3-aux (cdr l) (+ s (car l)) (+ n 1))
        (if (zero? n) 0 (/ s n))))
  (avg3-aux l 0 0))

(average3 (intervalle 1 11))

;;; Q4

;;; myreduce: combinateur de réduction
(define (myreduce f res l)
  (if (pair? l)
      (myreduce f (f res (car l)) (cdr l))
      res))

(define (average4 l)
  ;; on utilise myreduce avec pour accumulateur la paire (sum, len)
  (let ((res (myreduce
              ;; La fonction prends en argument l'accu (sum, len),
              ;; et un élément x de l.
              ;; On renvoie le nouvel accumulateur (sum + x, len + 1)
              (lambda (acc x) (list (+ (car acc) x) (+ (cadr acc) 1)))
              (list 0 0) l)))
    ;; a la fin de la réduction, res contient
    ;; (list (sum l) (length l))
    (if (zero? (cadr res)) 0 (/ (car res) (cadr res)))))

(average4 (intervalle 1 11))
