#lang racket

;;;exercice4

;;;Question 1
(define (constant? x )
  (number? x))

(define (operation? x )
  (if (pair? x)
      (if (symbol? (car x))
          #t
          (#f))
      #f))

(define (operation-op x)
  (if (operation? x)
      (car x)
      #f))

(define (operation-args x)
  (if (operation? x)
      (cdr x)
      #f))

;;;Question 2
;;; eval-op: fonctions de calcul et éléments neutres des opérateurs

(define (eval-op op)
  (case op
    ('+ (cons + 0))
    ('- (cons - 0))
    ('* (cons * 1))
    ('/ (cons / 1))
    (else (error "Wrong operator"))))


(define (myreduce f res l)
  (if (pair? l)
      (myreduce f (f res (car l)) (cdr l))
      res))

      
;;;; Exercice 5 : Expressions avec variables (à rendre)

;;; Q1

;;; Q2

;;; Q3

;;;; Exercice 6 : Abstractions et applications (challenge)

;;; Q1

;;; Q2

;;; Q3

;;; Q4
